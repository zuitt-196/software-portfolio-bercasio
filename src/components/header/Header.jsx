import React ,{useState, useRef} from 'react'
import css from './HeaderStyle.module.scss'
import {BiMenuAltRight, BiPhoneCall} from 'react-icons/bi'
import {motion} from 'framer-motion'
import { getMenuStyles, headerVariants } from '../../utils/motion'
import useHeaderShadow from '../../hooks/useHeaderShadow'
import useOutsideAlerter from '../../hooks/useOutsideAlerter'
const Header = () => {
    const [menuOpen, setMenuOpen] = useState(false);
    const menuRef = useRef()
    // const MenuOpen = () => setMenuOpen((prev) => !prev);
    const headerShadows = useHeaderShadow();
  
// DEFINE THE useOutsideAlerter

useOutsideAlerter(
    {   
        menuRef,
        setMenuOpen
    }
)

  return (
    <motion.div
    initial='hidden'    
    whileInView='show'
    variants={headerVariants}
    viewport={{once: true, amount: 0.25}}
    className={`paddings ${css.wrapper}`}
    style={{boxShadow: headerShadows}}
    >
            {/*NAME SECION*/}
        <div className={`flexCenter ${css.container}`}>
            <div className={css.name}>
                Vhong
            </div>

            {/* MENU SECTION  */}
            <ul className={`flexCenter ${css.menu}`} style={getMenuStyles(menuOpen)} ref={menuRef}>
                <li><a href="">About</a></li>
                <li><a href="#skilss">Experiences</a></li>
                <li><a href="#experiences">Certificate</a></li>
                <li><a href="#projects">Skills</a></li> 
                <li><a href="#certificate">Projects</a></li>
                <li> <a href="#blog">Blog</a></li>
                <li className={`flexCenter ${css.phone}`}>
                     <p>09102764396</p>
                     <BiPhoneCall size={"40px"}/>
                </li>
            </ul>

            {/* FOP MEDUIM AND SMALL SCREEN */}
            <div className={css.menuIcon} onClick={() => setMenuOpen((prev) => !prev)}>
                <BiMenuAltRight/>
            </div>
        </div>

    </motion.div>
  )
}

export default Header