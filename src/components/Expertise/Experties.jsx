import React from 'react'
import { projectExperience, WhatDoIHelp } from '../../utils/data'
import css from './ExpertiesStyle.module.scss'
import Seo from '../../assets/seo.png';
import {motion} from 'framer-motion'
import { fadeIn, staggerContainer } from '../../utils/motion.js';

const Experties = () => {
  return (
    <section className={css.wrapper}>
            <a className="anchor" href="" id="skilss"></a>
            <h2 className="topPaddings primaryText">Skills</h2>
        <div className={`paddings  yPaddings flexCenter innerWidth ${css.container}`}>
            
            {/* LEFT SIDE */}
            <div className={css.leftSide}>
                    <div className={css.seo}>
                        <div className='flexCenter secondaryText '>
                            <img src={Seo} alt="seo"  width={40} height={40} />
                        </div>    
                    <div>   
                        <span>SEO</span>    
                        <span>
                            I have a basic comprehension of Search Engine Optimization (SEO), which involves techniques to improve a website's visibility and ranking on search engines.
                        </span>
                    </div>              
               </div>
                  
                {projectExperience.map((exp, i) => (
                    <div className={css.experince} key={i}
                    >
                        <div className='flexCenter'  style={{background: exp.bg}}>
                            <exp.icon size={25} style={{color: exp.color}}/>
                            {/* <p>sadsad</p> */}
                        </div>
                        <div>
                                <span>{exp.name}</span>
                                <span>{exp.paragraph}</span>
                        </div>
                    </div>
                ))}
            </div>

           
        </div>
    </section>
  )
}

export default Experties
