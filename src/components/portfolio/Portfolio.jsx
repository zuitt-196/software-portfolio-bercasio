import React,{useState} from 'react';
import { projectFullStack, projectsFrontEnd } from '../../utils/data';
import css from './PortfolioStyle.module.scss';
import {FaGitlab} from "react-icons/fa";
import {AiOutlineArrowDown} from 'react-icons/ai';
import {BiShow} from "react-icons/bi"
import Swal from 'sweetalert2';


const Portfolio = () => {
  const showeAlert = () =>{
    Swal.fire({
      title:"Trouble in Deployment",
      text:"Navigating the Challenges of Full Stack Deployment on a Limited Budget",
      icon:"error",
      confirmButtonText:"OK"
    }).then((result) =>{
      if (result.isConfirmed) {
        console.log("asdasdasdasdas");
      }
    })
  }
  return (
    <section className={`paddings  ${css.wrapper}`}>
        <a  className="anchor" id="projects"></a>
        <div className={`innerWidth flexCenter ${css.container}`}>
        
        <div className={`flexCenter ${css.heading}`}>
              <div>
                    <span className="primaryText">
                          My works
                    </span>
                    <p style={{marginTop: "10px"}}>
                      As new B developer , I've been diligently creating projects to enhance my skills and expertise in the field
                    </p>
                </div>

                <span className="secondaryText">
                  Explore More Works
                </span>
    
        </div>

        {/* IMAGE SECTION  FRONT END*/}
        <div className={`${css.titleProject}`}>
          <span className="secondaryText">Front-end Projects</span> 
          <AiOutlineArrowDown size={23} className={css.arrow}/>
        </div>
        <div className={`flexCenter ${css.showCase}`}>
                {projectsFrontEnd?.map((project,i) =>(
                  <div key={i}>
                      <img src={project.img} alt="projects" />
                      <div className={`flexCenter ${css.details}`}>
                          <span className='secondaryText'>{project.title}</span>
                          <div>
                                <button>
                                  <BiShow size={20} color="white"/>
                                    <a className="secondaryText" href={project. live}>Live</a>
                                </button>   
                                <button>
                                  <FaGitlab size={20} color="#fca326"/>
                                    <a className="secondaryText" href={project.gitlab}>Gitlab</a>
                                </button>  
                          </div>
                                 
                      </div>
                </div> 
                ))}
        </div>

           {/* IMAGE SECTION  FULL STACK*/}
           <div className={`${css.titleProject}`}>
          <span className="secondaryText">Full-stack Projects</span> 
          <AiOutlineArrowDown size={23} className={css.arrow}/>
        </div>
        <div className={`flexCenter ${css.showCase}`}>
                {projectFullStack.map((project,i) =>(
                  <div key={i}>
                      <img src={project.img} alt="projects" />
                      <div className={`flexCenter ${css.details}`}>
                          <span className='secondaryText'>{project.title}</span>
                            
                          <div>
                                <button onClick={showeAlert}>
                                  <BiShow size={20} color="white"/>
                                    <a className="secondaryText" href={project. live}>Live</a>
                                    
                                    
                                </button>   
                                <button>
                                  <FaGitlab size={20} color="#fca326"/>
                                    <a className="secondaryText" href={project.gitlab}>Gitlab</a>
                                </button>  
                          </div>
                      </div>
                  </div> 
                ))}
        </div>


        </div>
    </section>
  )
}

export default Portfolio
