import React from 'react'
import css from './HeroStyle.module.scss'
import imgCertificate from '../../assets/medal.png'
import person from '../../assets/person3.png'
import { staggerContainer } from '../../utils/motion'
// import {AiFillSafetyCertificate} from 'react-icons/pi'
import {motion} from 'framer-motion'
import pdf from '../../assets/Brown-Cream-Professional-Web-Developer-Resume-7.pdf';
import {FaHandPointer} from 'react-icons/fa'



const Hero = () => {
  return (
    <section className={`paddings ${css.wrapper}`}>
            <motion.div className={`innerWidth ${css.container}`}
            variants={staggerContainer}
            initial="hidden"
            whileInViewport="show"
            viewport={{once:false, amount:0.25}} >
                
                {/* UPPER ELEMENT */}
                <div className={css.upperElement}>
                        <span className="primaryText">Hi There , <br /> I'am Vhong</span>
                        <span className='secondaryText'> Dedicated to Coding <br />and Aspiring to be Successful Developer <br />
                        <a href={pdf} download={pdf}>
                        Click my Resume</a> <br />
                        <FaHandPointer />
                        </span> 
                      
                </div>

                {/* PERSON SECTION */}
                    <div className={css.person}>
                        <img src={person} alt="person" />
                    </div>
                <a className={css.email} href="mailto:bercaiobongbong@gmail.com">bercasiobongbong@gmail.com</a>    

                {/* lOWER ELEMET */}
                <div className={css.lowerElement}>
                    <div className={css.experience}>
                            <div className="primaryText">6</div>
                            <div className="secondaryText">
                                    <div>months</div>
                                    <div>Experience</div>
                            </div>
                    </div>

                    <div className={css.certificate}>
                        <img src={imgCertificate} alt="certificate" />
                        <span>TESDA CERTIFICATE</span>
                        <span>ZUITT BOOTCAMP CERTIFICATE</span>
                    </div>
                </div>  
            </motion.div>
    </section>
  )
}

export default Hero