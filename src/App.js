
import './App.css';
import css from  './styles/app.module.scss'
import Header from './components/header/Header';
import Hero from './components/hero/Hero';
import WorkExperience from './components/work/WorkExperience';
import Certifacte from './components/certificate/Certifacte';
import Experties from './components/Expertise/Experties'
import Portfolio from './components/portfolio/Portfolio'
import Moments from './components/moments/Moments';
import Footer from './components/footer/Footer'


function App() {
  return (
    <div className={`bg-primary ${css.container}`}>
        <Header/>
        <Hero/>
        <WorkExperience/>
        <Certifacte/>
        <Experties/>
        <Portfolio/>
        <Moments/>
        <Footer/>

    </div>
  );
}

export default App;
